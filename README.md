# MyQuiz

## Description rapide

L'objectif est de réaliser une application de Web dynamique permettant de jouer à des Quizz thématiques.

## Contexte

> Le descriptif de la mise en situation pro, afin que l'apprenant traduise le besoin en livrables

L'entreprise ChoseYourAnswer souhaite un système de quizz par thématique. Votre objectif est de fournir une application permettant de choisir et effectuer un quizz sur une thématique. 
Un utilisateur choisi sa thématique. À ce moment là, il aura accès aux question du quizz. Chaque question comporte 4 réponses possibles, et une seule réponse est correcte. L'utilisateur choisi sa réponse  et confirme son choix. 
Pour chaque réponse donnée, l'utilisateur aura un retour pour lui indiquer s'il a répondu juste. Si sa réponse est fausse, il devra avoir accès à la réponse correcte. À la fin du quizz l'utilisateur aura son score final. 

Les questions peuvent être toutes affichées sur un même page, mais dans l'idéal, elles sont affichées une par une. 

Idéalement, les questions sont affichées dans un ordre aléatoire et les réponses possibles aussi. 

Si possible, vous ferez en sorte que les thématiques et les questions soient chargées dynamiquement depuis un backend. 

## Livrables attendus

Vous livrez un lien vers un dépôt git (sur la plateforme de votre choix). Ce dépôt contiendra un fichier README indiquant les fonctionnalités réalisées et les ressources utilisées.
Vous prendrez soin de commenter votre code et documenter les éventuelles fonctions que vous aurez créé. 


## Modalité pédagogique


Réalisation individuelle, à réaliser en maximum 3 demi-journées. 


## Ressources (titre + (url ou PJ))

[Exemple d'application AJAX à adapter](https://gitlab.com/formation-coder/dwwm-2022/-/tree/master/sprint04/php_kickoff/ajax_serveur)

## Critères de performance

> Exemple : "Le résultat final doit avoir exactement les même composants d'interface utilisateur" ou "Le code produit doit respecter au moins les sections HTML et CSS de tel ou tel site"


Votre code HTML respectera le bonnes pratiques et sera valide. 

Votre application doit être responsive.



## Modalités d'évaluation

Le code source fourni sera évalué par les formateurs. 