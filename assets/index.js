const body = document.querySelector("body");
const section = document.querySelector("section");
const allTheme = document.getElementById("theme");
const h1 = document.querySelector("h1");
const quiz = document.getElementById("quiz");
const answerEls = document.querySelectorAll(".answer");
const questionEl = document.getElementById("question");
const a_text = document.getElementById("a_text");
const b_text = document.getElementById("b_text");
const c_text = document.getElementById("c_text");
const d_text = document.getElementById("d_text");
const submitBtn = document.getElementById("submit");
const heart1 = document.getElementById("heart1");
const heart2 = document.getElementById("heart2");
const heart3 = document.getElementById("heart3");

let currentQuiz = 0;
let score = 0;
let hp = 3;
let currentQuizData;
let clickedTheme;
let quizData;

allTheme.addEventListener("click", (e) => {
  // Affiche la section de jeu lorsqu'on clique
  if (quiz.classList.contains("hide")) {
    quiz.classList.remove("hide");
    quiz.classList.add("quiz-container");
  }
  if (!section.classList.contains("hide")) {
    section.classList.add("hide");
    h1.classList.add("hide");
  }

  const elem = e.target.parentNode;
  // On récupère l'id de l'article
  clickedTheme = elem.parentNode.id;

  fetch(`themes.json`)
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Erreur lors de la récupération des données");
      }
    })
    .then((data) => {
      quizData = data;
      loadQuiz(clickedTheme);
    })
    .catch((err) => {
      console.log(err);
    });
});

/**
 * Permet l'affichage des questions et réponses sur la page
 * @param clickedTheme le theme selectionné
 */
function loadQuiz(clickedTheme) {
  deselectAnswers();

  if (currentQuiz === quizData[clickedTheme].length) {
    displayEndScreen();
  }

  currentQuizData = quizData[clickedTheme][currentQuiz];

  questionEl.innerText = currentQuizData.question;
  a_text.innerText = currentQuizData.a;
  b_text.innerText = currentQuizData.b;
  c_text.innerText = currentQuizData.c;
  d_text.innerText = currentQuizData.d;
}

/**
 * Récupère la réponse du user
 * @returns answer 'String' qui est la réponse choisie par le user
 */
function getSelected() {
  let answer = undefined;

  answerEls.forEach((answerEl) => {
    if (answerEl.checked) {
      answer = answerEl.id;
    }
  });

  return answer;
}



// Envoi de la réponse à chaque question
submitBtn.addEventListener("click", (e) => {
  e.preventDefault();
  // On regarde la réponse
  const answer = getSelected();

  // On vérifie si elle est vide
  if (answer) {
    // On vérifie si la réponse est juste
    if (answer == currentQuizData.correct) {
      score++;
      currentQuiz++;
      loadQuiz(clickedTheme);
    }
    // Si la réponse est fausse
    else {
      // On perd une vie
      hp--;
      if (hp === 0) {
        displayEndScreen();
      } else {
        updateHearts();
      }
    }
    deselectAnswers();
  }
});

function updateHearts() {
  if (hp === 2) {
    heart3.style.visibility = "hidden";
  } else if (hp === 1) {
    heart2.style.visibility = "hidden";
  }
}

/**
 * Évite que la réponse choisie par le user reste selectionnée lorsqu'il valide sa réponse
 */
function deselectAnswers() {
  answerEls.forEach((answerEl) => {
    answerEl.checked = false;
  });
}

/**
 * Affiche l'écran de fin avec le score
 */
function displayEndScreen() {
  quiz.innerHTML = `
    <h1>Fin !</h1>
    <p>Votre score est de ${score}/${quizData[clickedTheme].length}</p>
    <button class="buttonQuiz" id="restart-btn">Retour aux thèmes</button>
  `;

  const restartBtn = document.getElementById("restart-btn");
  restartBtn.addEventListener("click", () => {
    location.reload();
  });
}
